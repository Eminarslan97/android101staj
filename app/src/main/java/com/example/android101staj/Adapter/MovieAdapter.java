package com.example.android101staj.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.android101staj.Movie;
import com.example.android101staj.R;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.List;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MovieViewHolder> implements Filterable {
    List<Movie> movies ;
    List<Movie> ListFull;
    private OnMovieListener mOnMovieListener;
    public MovieAdapter(List<Movie> movies, OnMovieListener onMovieListener){
        this.movies = movies;
        this.ListFull = movies;
        this.mOnMovieListener = onMovieListener;
    }
    @NonNull
    @Override
    public MovieAdapter.MovieViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card,parent,false);
        MovieViewHolder viewHolder = new MovieViewHolder(view, mOnMovieListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MovieAdapter.MovieViewHolder holder, int position) {
        Picasso.get().load(ListFull.get(position).getImgSrc()).into(holder.mImage);
    }

    @Override
    public int getItemCount() {
        return ListFull.size();
    }

    public class MovieViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView mImage;
        OnMovieListener onMovieListener;

        public MovieViewHolder(@NonNull View itemView , OnMovieListener onMovieListener) {
            super(itemView);
            mImage = itemView.findViewById(R.id.image);
            this.onMovieListener = onMovieListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            onMovieListener.OnMovieClick(getAdapterPosition());
        }
    }

    public interface OnMovieListener{
        void OnMovieClick(int position);
    }

    public Filter getFilter(){
        return exampleFilter;
    }

    Filter exampleFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            String filterPattern = charSequence.toString();

            if(filterPattern.isEmpty()){
                ListFull=movies;
            }else{
                List<Movie> filteredList = new ArrayList<>();
                for(Movie item : movies){
                    if(item.getName().toLowerCase().contains(filterPattern)){
                        filteredList.add(item);
                    }
                }
                ListFull = filteredList;
            }
            FilterResults results = new FilterResults();
            results.values = ListFull;
            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults results) {
            ListFull = (ArrayList<Movie>) results.values;
            notifyDataSetChanged();
        }
    };
}
