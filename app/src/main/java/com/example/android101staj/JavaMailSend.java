package com.example.android101staj;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class JavaMailSend extends AsyncTask<Void,Void,String> {

    private Context context;
    private Session session;


    private String to;
    private String subject;
    private String message;

    public JavaMailSend(Context context, String to, String subject, String message){
        this.context = context;
        this.to = to;
        this.subject = subject;
        this.message = message;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(Void... voids) {

        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

        session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {

                    // Yapılandırma verilerini bu şekilde alıp mailimize giriş yapıyoruz
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication("eminars97@gmail.com", "upumqwwiarnaxyce");
                    }

                });

        try {

            MimeMessage mm = new MimeMessage(session);
            mm.setFrom(new InternetAddress("eminars97@gmail.com"));
            mm.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            mm.setSubject(subject);
            mm.setContent(message, "text/plain; charset=UTF-8");
            Transport.send(mm);
            return "basarili";
        } catch (MessagingException e) {
            Log.e("hata","Hata : "+e.toString());
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(String sonuc) {
        if(sonuc != null){
            Toast.makeText(context,"İlgili Bilgiler Mailinize Gönderildi!",Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(context,"İşlem Başarısız, Daha Sonra Tekrar Deneyiniz!",Toast.LENGTH_SHORT).show();
        }
    }
}
