package com.example.android101staj.Fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;
import com.example.android101staj.Activity.MovieDetailActivity;
import com.example.android101staj.Activity.LoginActivity;
import com.example.android101staj.Movie;
import com.example.android101staj.Adapter.MovieAdapter;
import com.example.android101staj.R;
import com.example.android101staj.keyUtil;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemSelected;
import maes.tech.intentanim.CustomIntent;


public class MovieFragment<oncre> extends Fragment implements MovieAdapter.OnMovieListener {
    List<Movie> movies= new ArrayList<>();
    List<Movie> moviescpy= new ArrayList<>();
    List<Movie> favMovies= new ArrayList<>();
    FirebaseFirestore firebaseFirestore;
    FirebaseAuth fAuth = FirebaseAuth.getInstance();
    String user_id = fAuth.getUid();
    MovieAdapter movieAdapter;
    @BindView(R.id.ShortSpinner) Spinner mSpinner;
    boolean control;
    String cntrl;
    int shortControl=1;
    public MovieFragment(){

    }

    @OnItemSelected(R.id.ShortSpinner) void onItemSelected(int position) {
        if(position == 0 && shortControl ==0){
            shortControl=1;
            getActivity().recreate();
        }
        if(position == 1){
            Collections.sort(movies);
            movieAdapter.notifyDataSetChanged();
            shortControl=0;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        loadNightModeState();
        if(cntrl.equals(keyUtil.NIGHTTHEME) ){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }else if(cntrl.equals(keyUtil.LIGHTTHEME)){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        View MovieView = inflater.inflate(R.layout.fragment_movie, container, false);

        ButterKnife.bind(this, MovieView);
        ArrayAdapter adapter = ArrayAdapter.createFromResource(this.getContext(),R.array.ShortItem,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(adapter);
        RecyclerView myMovieList = (RecyclerView) MovieView.findViewById(R.id.movies_list);
        myMovieList.setHasFixedSize(true);
        myMovieList.setLayoutManager(new GridLayoutManager(getContext(),2));
        firebaseFirestore= FirebaseFirestore.getInstance();
        movieAdapter = new MovieAdapter(getDataFromFirestore(),this);
        myMovieList.setAdapter(movieAdapter);
        setHasOptionsMenu(true);
        return MovieView;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.main_menu,menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        menu.findItem(R.id.action_language).setVisible(false);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newtext) {
                if(newtext.isEmpty()){
                    moviescpy=movies;
                }else {
                    List<Movie> filteredList = new ArrayList<>();
                    for (Movie item : movies) {
                        if (item.getName().toLowerCase().contains(newtext.toLowerCase())) {
                            filteredList.add(item);
                        }
                    }
                    moviescpy = filteredList;
                }
                movieAdapter.getFilter().filter(newtext);
                return false;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_setings:
                showChangeTheme();
                break;
            case R.id.action_exit:
                fAuth.signOut();
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    private void showChangeTheme() {
        String [] langList = new String[]{keyUtil.LIGHT,keyUtil.NIGHT};
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
        mBuilder.setTitle(getText(R.string.SELECT_MODE));
        mBuilder.setSingleChoiceItems(langList, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(i == 0){
                    setNightModeState(keyUtil.LIGHTTHEME);
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                }
                else if(i == 1){
                    setNightModeState(keyUtil.NIGHTTHEME);
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                }
                dialogInterface.dismiss();
            }
        });
        AlertDialog mDialog = mBuilder.create();
        mDialog.show();
    }

    private List<Movie> getDataFromFirestore(){
        CollectionReference documentReference = firebaseFirestore.collection(keyUtil.FAVORITES).document(user_id).collection(keyUtil.MOVIES);
        documentReference.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if(e != null){
                    Toast.makeText(getActivity(),e.getLocalizedMessage().toString(),Toast.LENGTH_LONG).show();
                }
                favMovies.clear();
                if(queryDocumentSnapshots != null){
                    for(DocumentSnapshot snapshot : queryDocumentSnapshots.getDocuments()) {
                        Map<String,Object> data = snapshot.getData();
                        String favmovieid = snapshot.getId();
                        String name = (String) data.get(keyUtil.NAME);
                        String image = (String) data.get(keyUtil.IMAGE);
                        String type = (String) data.get(keyUtil.TYPE);
                        String date = (String) data.get(keyUtil.DATE);
                        String abstrack = (String) data.get(keyUtil.ABSTRACK);
                        String fragmanID = (String) data.get(keyUtil.FRAGMANID);
                        favMovies.add(new Movie(name,image,date,abstrack,type,fragmanID,favmovieid));
                    }
                }
            }
        });
        CollectionReference collectionReference = firebaseFirestore.collection(keyUtil.MOVIES);
        collectionReference.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if(e != null){
                    Toast.makeText(getActivity(),e.getLocalizedMessage().toString(),Toast.LENGTH_LONG).show();
                }
                DocumentReference ref = firebaseFirestore.collection(keyUtil.MOVIES).document(keyUtil.CONTROL);
                ref.delete();
                movies.clear();
                if(queryDocumentSnapshots != null){
                    for(DocumentSnapshot snapshot : queryDocumentSnapshots.getDocuments()){
                        Map<String,Object> data = snapshot.getData();
                        String movieid = snapshot.getId();
                        control = true;
                        for(Movie favMovie : favMovies){
                            if(favMovie.getId().equals(movieid)){
                                control = false;
                            }
                        }
                        if(control == true){
                            String name = (String) data.get(keyUtil.NAME);
                            String image = (String) data.get(keyUtil.IMAGE);
                            String type = (String) data.get(keyUtil.TYPE);
                            String date = (String) data.get(keyUtil.DATE);
                            String abstrack = (String) data.get(keyUtil.ABSTRACK);
                            String fragmanID = (String) data.get(keyUtil.FRAGMANID);
                            movies.add(new Movie(name,image,date,abstrack,type,fragmanID,movieid));
                            movieAdapter.notifyDataSetChanged();
                        }
                    }
                    moviescpy=movies;
                    movieAdapter.notifyDataSetChanged();
                }
            }
        });
        return movies;
    }
    public void setNightModeState(String state){
        SharedPreferences.Editor editor = getActivity().getSharedPreferences(keyUtil.THEMESHAREDNAME,getContext().MODE_PRIVATE).edit();
        editor.putString(keyUtil.THEMESHAREDFILENAME,state);
        editor.apply();
    }
    public void loadNightModeState(){
        SharedPreferences prefs = getActivity().getSharedPreferences(keyUtil.THEMESHAREDNAME, getContext().MODE_PRIVATE);
        cntrl =prefs.getString(keyUtil.THEMESHAREDFILENAME,"");
    }

    @Override
    public void OnMovieClick(int position) {
        Intent intent = new Intent(getActivity(), MovieDetailActivity.class);
        String cpyname = moviescpy.get(position).getName();
        String cpyimage = moviescpy.get(position).getImgSrc();
        String cpydate = moviescpy.get(position).getDate();
        String cpytype = moviescpy.get(position).getType();
        String cpyabstrack = moviescpy.get(position).getAbstrack();
        String cpyfragmanID = moviescpy.get(position).getFragmanID();
        String cpyID = moviescpy.get(position).getId();
        Bundle bundle = new Bundle();
        bundle.putString(keyUtil.NAME,cpyname);
        bundle.putString(keyUtil.IMAGE,cpyimage);
        bundle.putString(keyUtil.DATE,cpydate);
        bundle.putString(keyUtil.TYPE,cpytype);
        bundle.putString(keyUtil.ABSTRACK,cpyabstrack);
        bundle.putString(keyUtil.FRAGMANID,cpyfragmanID);
        bundle.putString(keyUtil.MOVIEID,cpyID);
        intent.putExtras(bundle);
        startActivity(intent);
        CustomIntent.customType(getContext(),keyUtil.ANIMLTR);
    }
}