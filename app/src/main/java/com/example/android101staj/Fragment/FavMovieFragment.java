package com.example.android101staj.Fragment;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.example.android101staj.Activity.MovieDetailActivity;
import com.example.android101staj.Activity.LoginActivity;
import com.example.android101staj.Movie;
import com.example.android101staj.Adapter.MovieAdapter;
import com.example.android101staj.R;
import com.example.android101staj.keyUtil;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import butterknife.ButterKnife;
import maes.tech.intentanim.CustomIntent;

public class FavMovieFragment extends Fragment implements MovieAdapter.OnMovieListener {
    List<Movie> movies= new ArrayList<>();
    List<Movie> moviescpy= new ArrayList<>();
    private FirebaseFirestore firebaseFirestore;
    FirebaseAuth fAuth = FirebaseAuth.getInstance();
    MovieAdapter movieAdapter;
    String user_id = fAuth.getUid();

    public FavMovieFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View FavMoviesFragment = inflater.inflate(R.layout.fragment_fav_movie, container, false);
        RecyclerView myFavMovieList = (RecyclerView) FavMoviesFragment.findViewById(R.id.fav_movies_list);
        ButterKnife.bind(this, FavMoviesFragment);
        myFavMovieList.setHasFixedSize(true);
        myFavMovieList.setLayoutManager(new GridLayoutManager(getContext(),2));
        firebaseFirestore= FirebaseFirestore.getInstance();
        movieAdapter = new MovieAdapter(getDataFromFirestore(),this);
        myFavMovieList.setAdapter(movieAdapter);
        setHasOptionsMenu(true);
        return FavMoviesFragment;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.main_menu,menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        menu.findItem(R.id.action_language).setVisible(false);
        menu.findItem(R.id.action_setings).setVisible(false);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newtext) {
                if(newtext.isEmpty()){
                    moviescpy=movies;
                }else {
                    List<Movie> filteredList = new ArrayList<>();
                    for (Movie item : movies) {
                        if (item.getName().toLowerCase().contains(newtext.toLowerCase())) {
                            filteredList.add(item);
                        }
                    }
                    moviescpy = filteredList;
                }
                movieAdapter.getFilter().filter(newtext);
                return false;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_exit:
                fAuth.signOut();
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private List<Movie> getDataFromFirestore(){
        CollectionReference collectionReference = firebaseFirestore.collection(keyUtil.FAVORITES).document(user_id).collection(keyUtil.MOVIES);
        collectionReference.addSnapshotListener(new EventListener<QuerySnapshot>() {

            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if(e != null){
                    Toast.makeText(getActivity(),e.getLocalizedMessage().toString(),Toast.LENGTH_LONG).show();
                }
                movies.clear();
                if(queryDocumentSnapshots != null){
                    for(DocumentSnapshot snapshot : queryDocumentSnapshots.getDocuments()){
                        Map<String,Object> data = snapshot.getData();
                        String id = snapshot.getId();
                        String name = (String) data.get(keyUtil.NAME);
                        String image = (String) data.get(keyUtil.IMAGE);
                        String type = (String) data.get(keyUtil.TYPE);
                        String date = (String) data.get(keyUtil.DATE);
                        String abstrack = (String) data.get(keyUtil.ABSTRACK);
                        String fragmanID = (String) data.get(keyUtil.FRAGMANID);
                        movies.add(new Movie(name,image,date,abstrack,type,fragmanID,id));
                        movieAdapter.notifyDataSetChanged();
                    }
                }
                moviescpy=movies;
                movieAdapter.notifyDataSetChanged();
            }
        });
        return movies;
    }

    @Override
    public void OnMovieClick(int position) {
        Intent intent = new Intent(getActivity(), MovieDetailActivity.class);
        String cpyname = moviescpy.get(position).getName();
        String cpyimage = moviescpy.get(position).getImgSrc();
        String cpydate = moviescpy.get(position).getDate();
        String cpytype = moviescpy.get(position).getType();
        String cpyabstrack = moviescpy.get(position).getAbstrack();
        String cpyfragmanID = moviescpy.get(position).getFragmanID();
        String cpyID = moviescpy.get(position).getId();
        Bundle bundle = new Bundle();
        bundle.putString(keyUtil.NAME,cpyname);
        bundle.putString(keyUtil.IMAGE,cpyimage);
        bundle.putString(keyUtil.DATE,cpydate);
        bundle.putString(keyUtil.TYPE,cpytype);
        bundle.putString(keyUtil.ABSTRACK,cpyabstrack);
        bundle.putString(keyUtil.FRAGMANID,cpyfragmanID);
        bundle.putString(keyUtil.MOVIEID,cpyID);
        intent.putExtras(bundle);
        startActivity(intent);
        CustomIntent.customType(getContext(),keyUtil.ANIMLTR);
    }
}