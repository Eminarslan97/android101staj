package com.example.android101staj.Fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;
import com.example.android101staj.Activity.LoginActivity;
import com.example.android101staj.Movie;
import com.example.android101staj.Adapter.MovieAdapter;
import com.example.android101staj.R;
import com.example.android101staj.Activity.SequenceDetailActivity;
import com.example.android101staj.keyUtil;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemSelected;
import maes.tech.intentanim.CustomIntent;


public class SequenceFragment extends Fragment implements MovieAdapter.OnMovieListener {
    List<Movie> sequences= new ArrayList<>();
    List<Movie> sequencescpy= new ArrayList<>();
    List<Movie> favMovies= new ArrayList<>();
    private FirebaseFirestore firebaseFirestore;
    FirebaseAuth fAuth = FirebaseAuth.getInstance();
    @BindView(R.id.ShortSpinner) Spinner mSpinner;
    MovieAdapter sequenceAdapter;
    String user_id = fAuth.getUid();
    boolean control;
    int shortControl=1;

    public SequenceFragment(){

    }

    @OnItemSelected(R.id.ShortSpinner) void onItemSelected(int position) {
        if(position == 0 && shortControl ==0){
            shortControl=1;
            getFragmentManager().beginTransaction()
                    .replace(R.id.fragment_movie, new SequenceFragment()).commit();
        }
        if(position == 1){
            Collections.sort(sequences);
            sequenceAdapter.notifyDataSetChanged();
            shortControl=0;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View SequenceView = inflater.inflate(R.layout.fragment_sequence, container, false);
        RecyclerView mySequenceList = (RecyclerView) SequenceView.findViewById(R.id.sequence_list);
        ButterKnife.bind(this, SequenceView);
        ArrayAdapter adapter = ArrayAdapter.createFromResource(this.getContext(),R.array.ShortItem,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(adapter);
        mySequenceList.setLayoutManager(new GridLayoutManager(getContext(),2));
        firebaseFirestore= FirebaseFirestore.getInstance();
        sequenceAdapter = new MovieAdapter(getDataFromFirestore(),this);
        mySequenceList.setAdapter(sequenceAdapter);
        setHasOptionsMenu(true);
        return SequenceView;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.main_menu,menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        menu.findItem(R.id.action_language).setVisible(false);
        menu.findItem(R.id.action_setings).setVisible(false);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newtext) {
                if(newtext.isEmpty()){
                    sequencescpy=sequences;
                }else {
                    List<Movie> filteredList = new ArrayList<>();
                    for (Movie item : sequences) {
                        if (item.getName().toLowerCase().contains(newtext.toLowerCase())) {
                            filteredList.add(item);
                        }
                    }
                    sequencescpy = filteredList;
                }
                sequenceAdapter.getFilter().filter(newtext);
                return false;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_exit:
                fAuth.signOut();
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private List<Movie> getDataFromFirestore(){
        CollectionReference documentReference = firebaseFirestore.collection(keyUtil.FAVORITES).document(user_id).collection(keyUtil.SEQUENCES);
        documentReference.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if(e != null){
                    Toast.makeText(getActivity(),e.getLocalizedMessage().toString(),Toast.LENGTH_LONG).show();
                }
                favMovies.clear();
                if(queryDocumentSnapshots != null){
                    for(DocumentSnapshot snapshot : queryDocumentSnapshots.getDocuments()) {
                        Map<String,Object> data = snapshot.getData();
                        String favmovieid = snapshot.getId();
                        String name = (String) data.get(keyUtil.NAME);
                        String image = (String) data.get(keyUtil.IMAGE);
                        String type = (String) data.get(keyUtil.TYPE);
                        String date = (String) data.get(keyUtil.DATE);
                        String abstrack = (String) data.get(keyUtil.ABSTRACK);
                        String fragmanID = (String) data.get(keyUtil.FRAGMANID);
                        favMovies.add(new Movie(name,image,date,abstrack,type,fragmanID,favmovieid));
                    }
                }
            }
        });

        CollectionReference collectionReference = firebaseFirestore.collection(keyUtil.SEQUENCE);
        collectionReference.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Toast.makeText(getActivity(), e.getLocalizedMessage().toString(), Toast.LENGTH_LONG).show();
                }
                DocumentReference ref = firebaseFirestore.collection(keyUtil.SEQUENCE).document(keyUtil.CONTROL);
                ref.delete();
                sequences.clear();
                if (queryDocumentSnapshots != null) {
                    for (DocumentSnapshot snapshot : queryDocumentSnapshots.getDocuments()) {
                        Map<String, Object> data = snapshot.getData();
                        String sequenceID = snapshot.getId();
                        control = true;
                        for (Movie favMovie : favMovies) {
                            if (favMovie.getId().equals(sequenceID)) {
                                control = false;
                            }
                        }
                        if (control == true) {
                            String name = (String) data.get(keyUtil.NAME);
                            String image = (String) data.get(keyUtil.IMAGE);
                            String type = (String) data.get(keyUtil.TYPE);
                            String date = (String) data.get(keyUtil.DATE);
                            String abstrack = (String) data.get(keyUtil.ABSTRACK);
                            String fragmanID = (String) data.get(keyUtil.FRAGMANID);
                            sequences.add(new Movie(name, image, date, abstrack, type, fragmanID, sequenceID));
                            sequenceAdapter.notifyDataSetChanged();
                        }
                    }
                    sequencescpy=sequences;
                    sequenceAdapter.notifyDataSetChanged();
                }
            }
        });
        return sequences;
    }

    @Override
    public void OnMovieClick(int position) {
        Intent intent = new Intent(getActivity(), SequenceDetailActivity.class);
        String cpyname = sequencescpy.get(position).getName();
        String cpyimage = sequencescpy.get(position).getImgSrc();
        String cpydate = sequencescpy.get(position).getDate();
        String cpytype = sequencescpy.get(position).getType();
        String cpyabstrack = sequencescpy.get(position).getAbstrack();
        String cpyfragmanID = sequencescpy.get(position).getFragmanID();
        String cpyID = sequencescpy.get(position).getId();
        Bundle bundle = new Bundle();
        bundle.putString(keyUtil.NAME,cpyname);
        bundle.putString(keyUtil.IMAGE,cpyimage);
        bundle.putString(keyUtil.DATE,cpydate);
        bundle.putString(keyUtil.TYPE,cpytype);
        bundle.putString(keyUtil.ABSTRACK,cpyabstrack);
        bundle.putString(keyUtil.FRAGMANID,cpyfragmanID);
        bundle.putString(keyUtil.SEQUENCEID,cpyID);
        intent.putExtras(bundle);
        startActivity(intent);
        CustomIntent.customType(getContext(),keyUtil.ANIMLTR);
    }

}