package com.example.android101staj.Fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.android101staj.R;
import com.example.android101staj.Adapter.SectionPagerAdapter;
import com.example.android101staj.keyUtil;
import com.google.android.material.tabs.TabLayout;


public class FavFragment extends Fragment {
    View FavFragment;
    ViewPager viewPager;
    TabLayout tabLayout;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FavFragment = inflater.inflate(R.layout.fragment_fav, container, false);
        viewPager = FavFragment.findViewById(R.id.viewPager);
        tabLayout = FavFragment.findViewById(R.id.tabLayout);
        return FavFragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setUpViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void setUpViewPager(ViewPager viewPager) {
        SectionPagerAdapter adapter = new SectionPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new FavMovieFragment(), keyUtil.MOVIESTR);
        adapter.addFragment(new FavSequenceFragment(),keyUtil.SEQUENCESTR);
        viewPager.setAdapter(adapter);
    }
}