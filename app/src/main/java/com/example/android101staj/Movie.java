package com.example.android101staj;

import java.util.Comparator;

public class Movie implements Comparable<Movie>{

    String name;
    String imgSrc;
    String date;
    String abstrack;
    String type;
    String fragmanID;
    String Id;

    public Movie(){

    }

    public Movie(String name,  String imgSrc , String date , String abstrack , String type ,String fragmanID,String id) {
        this.name = name;
        this.imgSrc = imgSrc;
        this.date = date;
        this.abstrack = abstrack;
        this.type = type;
        this.fragmanID = fragmanID;
        this.Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImgSrc() {
        return imgSrc;
    }

    public void setImgSrc(String imgSrc) {
        this.imgSrc = imgSrc;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAbstrack() {
        return abstrack;
    }

    public void setAbstrack(String abstrack) {
        this.abstrack = abstrack;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFragmanID() {
        return fragmanID;
    }

    public void setFragmanID(String fragmanID) {
        this.fragmanID = fragmanID;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    @Override
    public int compareTo(Movie movie) {
        return this.name.compareTo(movie.getName());
    }
}
