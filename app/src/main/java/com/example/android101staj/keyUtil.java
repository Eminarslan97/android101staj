package com.example.android101staj;

public final class keyUtil {
    public static final String NAME = "name";
    public static final String IMAGE = "image";
    public static final String DATE = "date";
    public static final String TYPE = "type";
    public static final String ABSTRACK = "abstrack";
    public static final String FRAGMANID = "fragmanID";
    public static final String MOVIEID = "movieid";
    public static final String SEQUENCEID = "sequenceID";
    public static final String MOVIESTR = "Filmler";
    public static final String SEQUENCESTR = "Diziler";
    public static final String MOVIES = "movies";
    public static final String SEQUENCES = "sequences";
    public static final String SEQUENCE = "sequence";
    public static final String FAVORITES = "favorites";
    public static final String CONTROL = "control";
    public static final String NULL = "null";
    public static final String ANIMRTL = "right-to-left";
    public static final String ANIMLTR = "left-to-right";
    public static final String RANDOMNUMBER = "randomNumber";
    public static final String SUBJECT = "Konu";
    public static final String LOGINSHAREDNAME = "Settings";
    public static final String THEMESHAREDNAME = "filename";
    public static final String THEMESHAREDFILENAME = "NightMode";
    public static final String NIGHTTHEME = "NightTheme";
    public static final String LIGHTTHEME = "LightTheme";
    public static final String LIGHT = "Light";
    public static final String NIGHT = "Night";
    public static final String TURKISH = "Türkçe";
    public static final String ARABIC = "عربى";
    public static final String LOGINSHAREDLANGNAME = "My_Lang";
    public static final String POINT = "point";
    public static final String API_KEY = "AIzaSyCdfK_ggEw-uXxSaMPjMWkEUmQH1BhoQ-0";
}
