package com.example.android101staj.Activity;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import com.example.android101staj.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterActivity extends AppCompatActivity {
    @BindView(R.id.email) EditText mEmail;
    @BindView(R.id.fullName) EditText mFullName;
    @BindView(R.id.password) EditText mPassword;
    @BindView(R.id.phone) EditText mPhone;
    @BindView(R.id.progressBar) ProgressBar mProgressBar;
    @BindView(R.id.registerBtn) Button mRegisterBtn;
    @BindView(R.id.createTxt) TextView mCreatetxt;
    FirebaseAuth fAuth = FirebaseAuth.getInstance();
    @OnClick(R.id.registerBtn) void registerClick(View view){
        String email = mEmail.getText().toString().trim();
        String password = mPassword.getText().toString().trim();
        if(TextUtils.isEmpty(email)){
            mEmail.setError(getText(R.string.MAIL_ERROR));
            return;
        }
        if(TextUtils.isEmpty(password)){
            mPassword.setError(getText(R.string.PASSWORD_ERROR));
            return;
        }
        if(password.length() < 6){
            mPassword.setError(getText(R.string.PASSWORD_LENGTH_ERROR));
            return;
        }
        mProgressBar.setVisibility(View.VISIBLE);
        fAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    Toast.makeText(RegisterActivity.this, getText(R.string.SUCCESSFUL_REGISTER), Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                }else {
                    Toast.makeText(RegisterActivity.this, getText(R.string.NOT_SUCCESSFUL_REGISTER) , Toast.LENGTH_SHORT).show();
                    mProgressBar.setVisibility(View.GONE);
                }
            }
        });
    }

    @OnClick(R.id.createTxt) void loginClick(View view){
        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        if(fAuth.getCurrentUser() != null){
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
            finish();
        }
        ButterKnife.bind(this);
    }
}