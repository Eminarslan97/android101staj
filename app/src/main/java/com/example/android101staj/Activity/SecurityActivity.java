package com.example.android101staj.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.example.android101staj.R;
import com.example.android101staj.keyUtil;
import com.google.firebase.auth.FirebaseAuth;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SecurityActivity extends AppCompatActivity {
    @BindView(R.id.mailCheck) EditText mMailCheck;
    @BindView(R.id.time) TextView mTime;
    @BindView(R.id.mailCheckbtn) Button mMailCheckBtn;
    FirebaseAuth fAuth = FirebaseAuth.getInstance();
    int finalI=1;
    private Handler mainHandler = new Handler();
    ExampleRunnable runnable = new ExampleRunnable(100);
    Thread thread = new Thread(runnable);

    @OnClick(R.id.mailCheckbtn) void mailCheckbtnClick(View view) {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        int randomNumber = getIntent().getExtras().getInt(keyUtil.RANDOMNUMBER);
        if (mMailCheck.getText().toString().trim().equals("")) {
            Toast.makeText(SecurityActivity.this, getText(R.string.SECURITY_NULL_NUMBER), Toast.LENGTH_SHORT).show();
        } else{
            String code = mMailCheck.getText().toString().trim();
            int mMailCheckNumber = Integer.parseInt(code);
            if (mMailCheckNumber == randomNumber) {
                finalI = 0;
                startActivity(intent);
            } else {
                Toast.makeText(SecurityActivity.this, getText(R.string.SECURITY_FALSE_NUMBER), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void finish() {
        fAuth.signOut();
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(intent);
        super.finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_NO){
            setTheme(R.style.AppTheme);
        }
        if(AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES){
            setTheme(R.style.DarkTheme);
        }
        setContentView(R.layout.activity_security);
        thread.start();
        ButterKnife.bind(this);
    }

    class ExampleRunnable implements Runnable{
        int seconds;
        ExampleRunnable(int seconds){
            this.seconds = seconds;
        }
        @Override
        public void run() {
                for (int i = seconds; i >= 0; i--) {
                    if(finalI != 0)
                    finalI = i;
                    mainHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            mTime.setText((String) getText(R.string.SECURITY_TIME)+ finalI);
                            if (finalI == 0) {
                                fAuth.signOut();
                                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                startActivity(intent);
                            }
                        }
                    });
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (finalI == 0) {
                        break;
                    }
                }
        }
    }
}