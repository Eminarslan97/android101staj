package com.example.android101staj.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.example.android101staj.JavaMailSend;
import com.example.android101staj.R;
import com.example.android101staj.keyUtil;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import java.util.Locale;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {
    @BindView(R.id.email) EditText mEmail;
    @BindView(R.id.password) EditText mPassword;
    @BindView(R.id.progressBar) ProgressBar mprogressBar;
    @BindView(R.id.loginBtn) Button mLoginBtn;
    @BindView(R.id.createTxt) TextView mCreateBtn;
    @BindView(R.id.passReset) TextView mforgotTextLink;
    FirebaseAuth fAuth = FirebaseAuth.getInstance();

    @OnClick(R.id.loginBtn) void loginClick(View view){
        String email    = mEmail.getText().toString().trim();
        String password = mPassword.getText().toString().trim();
        if(TextUtils.isEmpty(email)){
            mEmail.setError(getText(R.string.MAIL_ERROR));
            return;
        }
        if(TextUtils.isEmpty(password)){
            mPassword.setError(getText(R.string.PASSWORD_ERROR));
            return;
        }
        if(password.length() < 6){
            mPassword.setError(getText(R.string.PASSWORD_LENGTH_ERROR));
            return;
        }
        mprogressBar.setVisibility(View.VISIBLE);

        fAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    Intent intent = new Intent(getApplicationContext(), SecurityActivity.class);
                    Bundle bundle = new Bundle();
                    Toast.makeText(LoginActivity.this, getText(R.string.SUCCESSFUL_LOGIN), Toast.LENGTH_SHORT).show();
                    String EMail = fAuth.getCurrentUser().getEmail();
                    int randomNumber = (int) (Math.random() * (999999 - 100000)) + 100000;
                    String mailText = (String) getText(R.string.LOGIN_MAIL_MESSAGE)+randomNumber;
                    JavaMailSend mg = new JavaMailSend(LoginActivity.this, EMail, keyUtil.SUBJECT, mailText);
                    mg.execute();
                    bundle.putInt(keyUtil.RANDOMNUMBER,randomNumber);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }else {
                    Toast.makeText(LoginActivity.this, getText(R.string.NOT_SUCCESSFUL_LOGIN), Toast.LENGTH_SHORT).show();
                    mprogressBar.setVisibility(View.GONE);
                }
            }
        });
    }

    @OnClick(R.id.createTxt) void registerClick(View view){
        startActivity(new Intent(getApplicationContext(), RegisterActivity.class));
    }

    @OnClick(R.id.passReset) void resetmailClick(View view){
        final EditText resetMail = new EditText(view.getContext());
        AlertDialog.Builder passwordResetDialog = new AlertDialog.Builder(view.getContext());
        passwordResetDialog.setTitle(getText(R.string.RESET_PASSWORD_1));
        passwordResetDialog.setMessage(getText(R.string.RESET_PASSWORD_2));
        passwordResetDialog.setView(resetMail);
        passwordResetDialog.setPositiveButton(getText(R.string.YES), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String mail = resetMail.getText().toString();
                fAuth.sendPasswordResetEmail(mail).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(LoginActivity.this, getText(R.string.SUCCESSFUL_RESET_PASSWORD), Toast.LENGTH_SHORT).show();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(LoginActivity.this, getText(R.string.NOT_SUCCESSFUL_RESET_PASSWORD) + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        passwordResetDialog.setNegativeButton(getText(R.string.NO), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        passwordResetDialog.create().show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadLocale();
        setContentView(R.layout.activity_login);
        if(fAuth.getCurrentUser() != null){
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
            finish();
        }
        ButterKnife.bind(this);
    }

    @Override
    public boolean onCreateOptionsMenu(@NonNull Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        menu.findItem(R.id.action_search).setVisible(false);
        menu.findItem(R.id.action_exit).setVisible(false);
        menu.findItem(R.id.action_setings).setVisible(false);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_language:
                showChangeLang();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showChangeLang() {
        String [] langList = new String[]{keyUtil.TURKISH,keyUtil.ARABIC};
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(LoginActivity.this);
        mBuilder.setTitle(getText(R.string.LOGIN_BUILDER_TITLE));
        mBuilder.setSingleChoiceItems(langList, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(i == 0){
                    setAppLocale("tr");
                    //AppCompatActivity.recreate();
                    Intent in = new Intent(LoginActivity.this, LoginActivity.class);
                    finish();
                    overridePendingTransition(0, 0);
                    startActivity(in);
                    overridePendingTransition(0, 0);
                }
                else if(i == 1){
                    setAppLocale("ar");
                    /*recreate();*/
                    Intent in = new Intent(LoginActivity.this, LoginActivity.class);
                    finish();
                    overridePendingTransition(0, 0);
                    startActivity(in);
                    overridePendingTransition(0, 0);
                }
                dialogInterface.dismiss();
            }
        });
        AlertDialog mDialog = mBuilder.create();
        mDialog.show();
    }
    private void setAppLocale(String localeCode){
        Locale locale = new Locale(localeCode);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,getBaseContext().getResources().getDisplayMetrics());
        SharedPreferences.Editor editor = getSharedPreferences(keyUtil.LOGINSHAREDNAME,MODE_PRIVATE).edit();
        editor.putString(keyUtil.LOGINSHAREDLANGNAME,localeCode);
        editor.apply();
    }
    public void loadLocale(){
        SharedPreferences prefs = getSharedPreferences(keyUtil.LOGINSHAREDNAME, Activity.MODE_PRIVATE);
        String language =prefs.getString(keyUtil.LOGINSHAREDLANGNAME,"");
        setAppLocale(language);
    }
}