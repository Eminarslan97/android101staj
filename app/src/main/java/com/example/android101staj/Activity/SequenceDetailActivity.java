package com.example.android101staj.Activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;

import com.example.android101staj.R;
import com.example.android101staj.keyUtil;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.squareup.picasso.Picasso;
import java.util.HashMap;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import maes.tech.intentanim.CustomIntent;

public class SequenceDetailActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener
        , YouTubePlayer.PlaybackEventListener,YouTubePlayer.PlayerStateChangeListener
{
    private final static int MAX_LINES_COLLAPSED = 3;
    private final boolean INITIAL_IS_COLLAPSED = true;
    private boolean isCollapsed = INITIAL_IS_COLLAPSED;
    FirebaseFirestore firebaseFirestore ;
    FirebaseAuth fAuth = FirebaseAuth.getInstance();
    String user_id = fAuth.getUid();
    @BindView(R.id.name_) TextView mNameYazdir;
    @BindView(R.id.name) TextView mName;
    @BindView(R.id.date) TextView mDate;
    @BindView(R.id.image) ImageView mImage;
    @BindView(R.id.type) TextView mType;
    @BindView(R.id.favBtn) ToggleButton mFavBtn;
    @BindView(R.id.abstrack) TextView mAbstrack;
    @BindView(R.id.fragman) YouTubePlayerView fragman;
    boolean control = false;
    String API_KEY =keyUtil.API_KEY;
    String video_url;

    @Override
    public void finish() {
        super.finish();
        CustomIntent.customType(this,keyUtil.ANIMRTL);
    }

    @OnClick(R.id.favBtn) void favBtnClick(View view){
        String sequenceID = getIntent().getExtras().getString(keyUtil.SEQUENCEID);
        DocumentReference ref = firebaseFirestore.collection(keyUtil.FAVORITES).document(user_id).collection(keyUtil.MOVIES).document(sequenceID);
        if(control == true) {
            firebaseFirestore.collection(keyUtil.FAVORITES).document(user_id).collection(keyUtil.SEQUENCES).document(sequenceID).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    FirebaseFirestore.getInstance().collection(keyUtil.FAVORITES).document(user_id).collection(keyUtil.SEQUENCES).document(sequenceID)
                            .delete()
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    Toast.makeText(getApplicationContext(),getText(R.string.ADD_FAV),Toast.LENGTH_SHORT).show();
                                    mFavBtn.setBackgroundResource(R.drawable.ic_favbtnborder);
                                    control = false;
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {

                        }
                    });
                }
            });
        }
        else if (control == false){
            HashMap<String,Object> movie = new HashMap<>();
            String name = getIntent().getExtras().getString(keyUtil.NAME);
            String image = getIntent().getExtras().getString(keyUtil.IMAGE);
            String type = getIntent().getExtras().getString(keyUtil.TYPE);
            String date = getIntent().getExtras().getString(keyUtil.DATE);
            String abstrack = getIntent().getExtras().getString(keyUtil.ABSTRACK);
            String point = getIntent().getExtras().getString(keyUtil.POINT);
            String fragmanID = getIntent().getExtras().getString(keyUtil.FRAGMANID);
            movie.put(keyUtil.NAME,name);
            movie.put(keyUtil.IMAGE,image);
            movie.put(keyUtil.TYPE,type);
            movie.put(keyUtil.DATE,date);
            movie.put(keyUtil.ABSTRACK,abstrack);
            movie.put(keyUtil.POINT,point);
            movie.put(keyUtil.FRAGMANID,fragmanID);
            firebaseFirestore.collection(keyUtil.FAVORITES).document(user_id).collection(keyUtil.SEQUENCES).document(sequenceID).set(movie).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Toast.makeText(getApplicationContext(),getText(R.string.DELETE_FAV),Toast.LENGTH_SHORT).show();
                    mFavBtn.setBackgroundResource(R.drawable.ic_favbtn);
                    control = true;
                }
            });
            DocumentReference documentReference = firebaseFirestore.collection(keyUtil.SEQUENCE).document(keyUtil.CONTROL);
            HashMap<String,Object> hashMap = new HashMap<>();
            hashMap.put(keyUtil.NULL,keyUtil.NULL);
            documentReference.set(hashMap).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {

                }
            });
        }
    }

    @OnClick(R.id.abstrack) void abstrackClick(View view){
        if (isCollapsed) {
            mAbstrack.setMaxLines(Integer.MAX_VALUE);
        } else {
            mAbstrack.setMaxLines(MAX_LINES_COLLAPSED);
        }
        isCollapsed = !isCollapsed;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_NO){
            setTheme(R.style.AppTheme);
        }
        if(AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES){
            setTheme(R.style.DarkTheme);
        }
        setContentView(R.layout.activity_sequence_detail);
        ButterKnife.bind(this);

        mFavBtn.setBackgroundResource(R.drawable.ic_favbtnborder);
        firebaseFirestore = FirebaseFirestore.getInstance();
        String name = getIntent().getExtras().getString(keyUtil.NAME);
        String image = getIntent().getExtras().getString(keyUtil.IMAGE);
        String type = getIntent().getExtras().getString(keyUtil.TYPE);
        String date = getIntent().getExtras().getString(keyUtil.DATE);
        String abstrack = getIntent().getExtras().getString(keyUtil.ABSTRACK);
        String point = getIntent().getExtras().getString(keyUtil.POINT);
        String fragmanID = getIntent().getExtras().getString(keyUtil.FRAGMANID);
        String sequenceID = getIntent().getExtras().getString(keyUtil.SEQUENCEID);
        CollectionReference collectionReference = firebaseFirestore.collection("favorites").document(user_id).collection("sequences");
        collectionReference.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if(e != null){
                    Toast.makeText(SequenceDetailActivity.this,e.getLocalizedMessage().toString(),Toast.LENGTH_LONG).show();
                }
                if(queryDocumentSnapshots != null)
                    for (DocumentSnapshot snapshot : queryDocumentSnapshots.getDocuments()) {
                        String getId = snapshot.getId();
                        if (getId.equals(sequenceID)) {
                            mFavBtn.setBackgroundResource(R.drawable.ic_favbtn);
                            control = true;
                            break;
                        }
                    }
            }
        });
        mNameYazdir.setText(name);
        Picasso.get().load(image).into(mImage);
        mName.setText(name);
        mDate.setText(date);
        mType.setText(type);
        mAbstrack.setText(abstrack);
        video_url=fragmanID;
        fragman.initialize(API_KEY,this);
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
        youTubePlayer.setPlayerStateChangeListener(this);
        youTubePlayer.setPlaybackEventListener(this);
        if(!b){
            youTubePlayer.cueVideo(video_url);
        }
    }

    @Override
    public void onPlaying() {

    }

    @Override
    public void onLoaded(String s) {

    }

    @Override
    public void onLoading() {

    }

    @Override
    public void onPaused() {

    }

    @Override
    public void onStopped() {

    }

    @Override
    public void onBuffering(boolean b) {

    }

    @Override
    public void onSeekTo(int i) {

    }

    @Override
    public void onAdStarted() {

    }

    @Override
    public void onVideoStarted() {

    }

    @Override
    public void onVideoEnded() {

    }

    @Override
    public void onError(YouTubePlayer.ErrorReason errorReason) {

    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

    }
}